function YBProductAnalytics(plugin as object) As Object

  ProductAnalyticsLog("Created YBProductAnalytics")
  this = CreateObject("roAssociativeArray")

  ' -------------------------------------------------------------------------------------------
  ' Attribute definition
  ' -------------------------------------------------------------------------------------------

  this._plugin = plugin
  this._initialized = false

  ' Settings

  this._productAnalyticsSettingsDefault = {
    "highlightContentAfter": 1,
    "enableStateTracking": false,
    "activeStateTimeout": 30,
    "activeStateDimension": 9,
  }

  this._productAnalyticsSettings = {}
  this._productAnalyticsSettings.Append(this._productAnalyticsSettingsDefault)

  ' Page

  this._page = ""

  ' Search

  this._searchQuery = invalid

  ' Content highlight

  this._contentHighlighted = invalid
  this._contentHighlightTimeout = CreateObject("roSGNode", "Timer")
  this._contentHighlightTimeout.setFields({"id": "timerHighlight", "duration": this._productAnalyticsSettings.highlightContentAfter, "repeat": false})
  this._contentHighlightTimeout.observeField("fire", this._plugin.port)
  
  this._pendingVideoEvents = []

  ' User state

  this._userStates = {
    active: "active",
    passive: "passive"
  }

  this._userStateTimeout = CreateObject("roSGNode", "Timer")
  this._userStateTimeout.setFields({"id": "timerUserState", "repeat": false})
  this._userStateDimension = ""
  this._userState = invalid       ' User state will be sent only when active or passive and never when invalid

  ' Event Types

  this._eventTypes = {
    userProfile: "User Profile",
    user: "User",
    navigation: "Navigation",
    attribution: "Attribution",
    section: "Section",
    contentPlayback: "Content Playback",
    search: "Search",
    externalApplication: "External Application",
    engagement: "Engagement",
    custom: "Custom Event"
  }

  ' Test

  if this._plugin = invalid
    ProductAnalyticsLog("Plugin reference unset.")
  end if

  ' -------------------------------------------------------------------------------------------
  ' Method definition
  ' -------------------------------------------------------------------------------------------

  ' Initializes product analytics
  ' @param {String} Screen name
  ' @param {Object} productAnalyticsSettings product analytics settings
  ' @param {Object} [npawtmConfigValue] configuration settings

  this.initialize = sub(page, productAnalyticsSettings)

    ' Set screen name

    if ( m._isString(page) )
      m._page = page
    else
      ProductAnalyticsLog("Page unset while initializing.")
    end if

    ' Load settings

    m._productAnalyticsSettings = {}
    m._productAnalyticsSettings.Append(m._productAnalyticsSettingsDefault)

    if ( Type(productAnalyticsSettings) = "roAssociativeArray")
      m._productAnalyticsSettings.Append(productAnalyticsSettings)
    end if

    ' Validate settings

    if ( getInterface(m._productAnalyticsSettings.highlightContentAfter, "ifInt") = invalid )
      m._productAnalyticsSettings.highlightContentAfter = m._productAnalyticsSettingsDefault["highlightContentAfter"]
    else if (m._productAnalyticsSettings.highlightContentAfter < 1)
      ProductAnalyticsLog("Invalid higlightContentAfter value. Using default value instead.")
      m._productAnalyticsSettings.highlightContentAfter = m._productAnalyticsSettingsDefault["highlightContentAfter"]
    end if

    if ( getInterface(m._productAnalyticsSettings.enableStateTracking, "ifBoolean") = invalid )
      m._productAnalyticsSettings.enableStateTracking = m._productAnalyticsSettingsDefault["enableStateTracking"]
    end if

    if ( getInterface(m._productAnalyticsSettings.activeStateTimeout, "ifInt") = invalid )
      m._productAnalyticsSettings.activeStateTimeout = m._productAnalyticsSettingsDefault["activeStateTimeout"]
    else if (m._productAnalyticsSettings.activeStateTimeout < 1)
      ProductAnalyticsLog("Invalid activeStateTimeout value. Using default value instead.")
      m._productAnalyticsSettings.activeStateTimeout = m._productAnalyticsSettingsDefault["activeStateTimeout"]
    end if

    if ( getInterface(m._productAnalyticsSettings.activeStateDimension, "ifInt") = invalid OR m._productAnalyticsSettings.activeStateDimension < 1 OR m._productAnalyticsSettings.activeStateDimension > 9)
      m._productAnalyticsSettings.activeStateDimension = m._productAnalyticsSettingsDefault["activeStateDimension"]
    end if

    ' Update highlight timer period
    
    m._contentHighlightTimeout.setField("duration", m._productAnalyticsSettings.highlightContentAfter)

    ' Update user state fields

    m._userStateTimeout.setField("duration", m._productAnalyticsSettings.activeStateTimeout)
    m._userStateTimeout.observeField("fire", m._plugin.port)
  
    m._userStateDimension = "content.customDimension." + StrI(m._productAnalyticsSettings.activeStateDimension, 10)

    ' Set as initialized

    m._initialized = true

    ' Start session

    if ( m._plugin = invalid )
      ProductAnalyticsLog("Cannot open a new session since plugin is unavailable")
    else
      m._plugin.eventHandler("sessionStart", {"page": m._page})
    end if

  end sub

  ' -------------------------------------------------------------------------------------------
  ' ADAPTER
  ' -------------------------------------------------------------------------------------------

  ' Track adapter start
  ' @private

  this._adapterTrackStart = sub()

    if m._initialized
      m.trackPlayerInteraction("start", {}, {}, true)
    end if

  end sub

  ' Execute after adapter is set to plugin

  this.adapterAfterSet = sub()

    ' Start tracking user state

    m._userStateStart()

  end sub

  ' Execute before removing adapter from plugin

  this.adapterBeforeRemove = sub()

    ' Stop tracking user state

    m._userStateStop()

    ' Discard pending video events
    
    m._pendingVideoEvents = []

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' SESSION
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' New user session

  this.newSession = sub()

    if m._initialized = false
      ProductAnalyticsLog("Cannot start a new session since Product Analytics is uninitialized.")
    else if m._plugin = invalid
      ProductAnalyticsLog("Cannot start a new session since plugin is unavailable.")
    else
      m.endSession()
      m._plugin.eventHandler("sessionStart", {"page": m._page})
    end if

  end sub

  ' Ends user session

  this.endSession = sub()

    if m._initialized = false
      ProductAnalyticsLog("Cannot end session since Product Analytics is uninitialized.")
    else if m._plugin = invalid
      ProductAnalyticsLog("Cannot end session since plugin is unavailable.")
    else
      m._plugin.eventHandler("sessionStop", {})
    end if

  end sub

  ' ---------------------------------------------------------------------------------------------
  ' LOGIN / LOGOUT
  ' ---------------------------------------------------------------------------------------------

  ' Successful login
  ' @param {string} userId The unique identifier of the user
  ' @param {string} [profileId] The unique identifier of the profile
  ' @param {string} [profileType] The profile type
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.loginSuccessful = sub (userId, profileId, profileType, dimensions, metrics)

    if ( NOT m._isString(userId) AND NOT m._isInteger(userId) )
      ProductAnalyticsLog("Cannot log in successfully since userId is unavailable.")
    else if (userId = "")
      ProductAnalyticsLog("Cannot log in successfully since userId is unset.")
    else if (m._checkState("log in successfully"))
      ' Send user login event
      
      m._fireEvent("User Login Successful", m._eventTypes.user, {username: userId}, dimensions, metrics)

      ' Send profile selection event (in case it has been supplied)

      if (NOT m._isString(profileId) OR profileId = "")
        profileId = invalid
      end if

      if (profileId <> invalid)
        m._userProfileSelectedEvent(profileId, profileType, dimensions, metrics)
      end if

      ' Set the userId (and profileId) options

      options = {"user.name": userId}

      if (profileId <> invalid)
        options["user.profileId"] = profileId
      end if

      m._setOptions(options)

      ' Close the current session and start a new one

      m.newSession()
    end if

  end sub

  ' Login unsuccessful
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.loginUnsuccessful = sub (dimensions, metrics)
    if ( m._checkState("log in unsuccessful") )
      ' Send an event informing that we are closing the session because of a profile change
      m._fireEvent("User Login Unsuccessful", m._eventTypes.user, {}, dimensions, metrics)
    end if
  end sub

  ' Logout
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.logout = sub (dimensions, metrics)

    if ( m._checkState("log out") )
      ' Send an event informing that we are closing the session

      m._fireEvent("User Logout", m._eventTypes.user, {}, dimensions, metrics)

      ' Delete the userId (and profileId) options
      ' Delete also option without dot (deprecated) just in case the user supplies it like this during initialization     

      m._deleteOptions(["username", "user.name", "user.profileId"])

      ' Close the current session and start a new one

      m.newSession()
    end if

  end sub

  ' ---------------------------------------------------------------------------------------------
  ' PROFILE
  ' ---------------------------------------------------------------------------------------------

  ' User profile created
  ' @param {string} profileId The unique identifier of the profile
  ' @param {string} [profileType] The profile type
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.userProfileCreated = sub (profileId, profileType, dimensions, metrics)

    if ( NOT m._isString(profileId) AND NOT m._isInteger(profileId))
      ProductAnalyticsLog("Cannot create user profile since profileId is unavailable.")
    else if (profileId = "")
      ProductAnalyticsLog("Cannot create user profile since profileId is unset.")
    else if ( m._checkState("create user profile") )
      dimensionsInternal = m._getUserProfileDimensions(profileId, profileType)
      m._fireEvent("User Profile Created", m._eventTypes.userProfile, dimensionsInternal, dimensions, metrics)
    end if

  end sub

  ' User profile selected
  ' @param {string} profileId The unique identifier of the profile
  ' @param {string} [profileType] The profile type
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.userProfileSelected = sub(profileId, profileType, dimensions, metrics)

    if ( NOT m._isString(profileId) AND NOT m._isInteger(profileId) )
      ProductAnalyticsLog("Cannot select user profile since profileId is unavailable.")
    else if (profileId = "")
      ProductAnalyticsLog("Cannot select user profile since profileId is unset.")
    else if (m._checkState("select user profile"))
      ' Send an event informing that we are closing the session because of a profile change

      m._userProfileSelectedEvent(profileId, profileType, dimensions, metrics)

      ' Set the profileId option

      m._setOptions({"user.profileId": profileId})

      ' Close the current session and open a new one

      m.newSession()
    end if

  end sub

  ' Fire user profile selected event
  ' @param {string} profileId The unique identifier of the profile
  ' @param {string} [profileType] The profile type
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track
  ' @private

  this._userProfileSelectedEvent = sub (profileId, profileType, dimensions, metrics)
    dimensionsInternal = m._getUserProfileDimensions(profileId, profileType)
    m._fireEvent("User Profile Selected", m._eventTypes.userProfile, dimensionsInternal, dimensions, metrics)
  end sub

  ' User profile deleted
  ' @param {string} profileId The unique identifier of the profile
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.userProfileDeleted = sub (profileId, dimensions, metrics)
    if (NOT m._isString(profileId) AND NOT m._isInteger(profileId))
      ProductAnalyticsLog("Cannot delete user profile since profileId is unavailable.")
    else if (profileId = "")
      ProductAnalyticsLog("Cannot delete user profile since profileId is unset.")
    else if (m._checkState("delete user profile"))
      dimensionsInternal = m._getUserProfileDimensions(profileId, invalid)
      m._fireEvent("User Profile Deleted", m._eventTypes.userProfile, dimensionsInternal, dimensions, metrics)
    end if
  end sub

  ' Get user profile dimensions
  ' @param {string} profileId
  ' @param {string} profileType
  ' @returns
  ' @private

  this._getUserProfileDimensions = function (profileId, profileType)
    dimensions = {
      "profileId": profileId
    }

    if (m._isString(profileType) AND profileType <> "")
      dimensions["profileType"] = profileType
    end if

    return dimensions
  end function

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' NAVIGATION
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Tracks navigation
  ' @param {string} page The unique name to identify a page of the application.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackNavByName = sub(page, dimensions, metrics)

    if (NOT m._isString(page) OR page = "")
      ProductAnalyticsLog("Cannot track navigation since page has not been supplied.")
    else if (m._checkState("track navigation"))
      m._page = page

      ' Track the event

      m._fireEvent("Navigation", m._eventTypes.navigation, {
        "paRoute": "",
        "paRouteDomain": "",
        "paFullRoute": "",
        "paReferrer": ""
      }, dimensions, metrics)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' ATTRIBUTION
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Tracks attribution
  ' @param {string} utmSource The UTM Source parameter. It is commonly used to identify a search engine, newsletter, or other source (i.e., Google, Facebook, etc.).
  ' @param {string} utmMedium The UTM Medium parameter. It is commonly used to identify a medium such as email or cost-per-click (cpc).
  ' @param {string} utmCampaign The UTM Campaign parameter. It is commonly used for campaign analysis to identify a specific product promotion or strategic campaign (i.e., spring sale).
  ' @param {string} utmTerm The UTM Term parameter. It is commonly used with paid search to supply the keywords for ads (i.e., Customer, NonBuyer, etc.).
  ' @param {string} utmContent The UTM Content parameter. It is commonly used for A/B testing and content-targeted ads to differentiate ads or links that point to the same URL (i.e., Banner1, Banner2, etc.)
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track


  this.trackAttribution = sub(utmSource, utmMedium, utmCampaign, utmTerm, utmContent, dimensions, metrics)

    if (m._checkState("track attribution"))

      params = {}

      if (m._isString(utmSource)) params["utmSource"] = utmSource
      if (m._isString(utmMedium)) params["utmMedium"] = utmMedium
      if (m._isString(utmCampaign)) params["utmCampaign"] = utmCampaign
      if (m._isString(utmTerm)) params["utmTerm"] = utmTerm
      if (m._isString(utmContent)) params["utmContent"] = utmContent

      ' Track attribution

      if params.Count() > 0

        params["utmUrl"] = ""

        m._fireEvent("Attribution", m._eventTypes.attribution, params, dimensions, metrics)

      end if
        
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' SECTION
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Section goes into viewport.
  ' @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
  ' @param {integer} sectionOrder The section order within the page.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackSectionVisible = sub(section, sectionOrder, dimensions, metrics)

    if (NOT m._isString(section) OR section = "")
      ProductAnalyticsLog("Cannot track section visible since no section has been supplied.")
    else if (NOT m._isInteger(sectionOrder) OR sectionOrder < 1)
      ProductAnalyticsLog("Cannot track section visible since sectionOrder is invalid.")
    else if (m._checkState("track section visible"))
      m._fireEvent("Section Visible", m._eventTypes.section, {
        "sectionOrder": sectionOrder,
        "section": section
      }, dimensions, metrics)
    end if

  end sub

  ' Section goes out of viewport.
  ' @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
  ' @param {integer} sectionOrder The section order within the page.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackSectionHidden = sub(section, sectionOrder, dimensions, metrics)

    if (NOT m._isString(section) OR section = "")
      ProductAnalyticsLog("Cannot track section hidden since no section has been supplied.")
    else if (NOT m._isInteger(sectionOrder) OR sectionOrder < 1)
      ProductAnalyticsLog("Cannot track section hidden since sectionOrder is invalid.")
    else if (m._checkState("track section hidden"))
      m._fireEvent("Section Hidden", m._eventTypes.section, {
        "sectionOrder": sectionOrder,
        "section": section
      }, dimensions, metrics)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' CONTENT
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Sends a content highlight event if content is focused during, at least, highlightContentAfter s.
  ' @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
  ' @param {integer} sectionOrder The section order within the page.
  ' @param {integer} column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
  ' @param {integer} row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
  ' @param {string} contentID The unique content identifier of the content linked.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.contentFocusIn = sub(section, sectionOrder, column, row, contentID, dimensions, metrics)
    m.contentFocusOut()

    if (NOT m._isString(section) OR section = "")
      ProductAnalyticsLog("Cannot track content highlight since no section has been supplied.")
    else if (NOT m._isInteger(sectionOrder) OR sectionOrder < 1)
      ProductAnalyticsLog("Cannot track content highlight since sectionOrder is invalid.")
    else if (NOT m._isInteger(column) OR column < 1)
      ProductAnalyticsLog("Cannot track content highlight since column is invalid.")
    else if (NOT m._isInteger(row) OR row < 1)
      ProductAnalyticsLog("Cannot track content highlight since row is invalid.")
    else if (NOT m._isString(contentID) OR contentID = "")
      ProductAnalyticsLog("Cannot track content highlight since no contentID has been supplied.")
    else if (m._checkState("track content highlight"))
      m._contentHighlighted = { "sectionOrder": sectionOrder, "section": section, "column": column, "row": row, "contentID": contentID, "dimensions": dimensions, "metrics": metrics }
      m._contentHighlightTimeout.control = "start"
    end if

  end sub

  ' Content loses focus
  ' @private

  this.contentFocusOut = sub()

    m._contentHighlighted = invalid
    m._contentHighlightTimeout.control = "stop"

  end sub

  ' Sends a content highlight event using selected content info
  ' @private

  this.trackContentHighlight = sub()

    if ( m._contentHighlighted = invalid )

      ProductAnalyticsLog("Cannot highlight content since no content is selected")

    else

      m._fireEvent("Section Content Highlight", m._eventTypes.section, {
        "sectionOrder": m._contentHighlighted["sectionOrder"],
        "section": m._contentHighlighted["section"],
        "column": m._contentHighlighted["column"],
        "row": m._contentHighlighted["row"],
        "contentId": m._contentHighlighted["contentID"]
      }, m._contentHighlighted["dimensions"], m._contentHighlighted["metrics"])

      m._contentHighlighted = invalid
      
    end if

  end sub


  ' Tracks the location of user clicks.
  ' @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
  ' @param {integer} sectionOrder The section order within the page.
  ' @param {integer} column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
  ' @param {integer} row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
  ' @param {string} contentID The unique content identifier of the content linked.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackContentClick = sub(section, sectionOrder, column, row, contentID, dimensions, metrics)

    if (NOT m._isString(section) or section = "")
      ProductAnalyticsLog("Cannot track content click since no section has been supplied.")
    else if (NOT m._isInteger(sectionOrder) or sectionOrder < 1)
      ProductAnalyticsLog("Cannot track content click since sectionOrder is invalid.")
    else if (NOT m._isInteger(column) or column < 1)
      ProductAnalyticsLog("Cannot track content click since column is invalid.")
    else if (NOT m._isInteger(row) or row < 1)
      ProductAnalyticsLog("Cannot track content click since row is invalid.")
    else if (NOT m._isString(contentID) or contentID = "")
      ProductAnalyticsLog("Cannot track content click since no contentID has been supplied.")
    else if (m._checkState("track content click"))
      m._fireEvent("Section Content Click", m._eventTypes.section, {
        "sectionOrder": sectionOrder,
        "section": section,
        "column": column,
        "row": row,
        "contentId": contentID
      }, dimensions, metrics)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' CONTENT PLAYBACK
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Tracks when a content starts playing be it automatically or through a user interaction.
  ' @param {string} contentID The unique content identifier of the content being played.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackPlay = sub(contentID, dimensions, metrics)

    eventName = "Content Play"
    startEvent = false

    if (NOT m._isString(contentID) OR contentID = "")
      ProductAnalyticsLog("Cannot track play since no contentID has been supplied.")
    else if (m._plugin.isStarted = false)
      m._pendingVideoEvents.push({ "eventName": eventName, "contentID": contentID, "dimensions": dimensions, "metrics": metrics, "startEvent": startEvent })
    else if (m._checkState("track play"))
      m._trackPlayerEventsPending()
      m._trackPlayerEvent(eventName, contentID, dimensions, metrics, startEvent)
    end if

  end sub

  ' Tracks content watching events.
  ' TODO: add (2nd) argument to tell whether user state must be updated or not
  ' @param {string} eventName The name of the interaction (i.e., Pause, Seek, Skip Intro, Skip Ads, Switch Language, etc.).
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track
  ' @param {boolean} [startEvent] Internal param informing that current interaction is responsible of first player start

  this.trackPlayerInteraction = sub(eventName, dimensions, metrics, startEvent)

    contentID = invalid

    if (NOT m._isBoolean(startEvent))
      startEvent = false
    end if

    if (NOT m._isString(eventName) OR eventName = "")
      ProductAnalyticsLog("Cannot track player interaction since no interaction name has been supplied.")
    else if (m._checkState("track player interaction"))
      
      eventNameFull = "Content Play " + eventName

      if ( m._plugin.isStarted = false )
        m._pendingVideoEvents.push({ "eventName": eventNameFull, "contentID": contentID, "dimensions": dimensions, "metrics": metrics, "startEvent": startEvent })
      else
        m._trackPlayerEventsPending()
        m._trackPlayerEvent(eventNameFull, contentID, dimensions, metrics, startEvent)
      end if

    end if

  end sub

  ' Track player pending events

  this._trackPlayerEventsPending = sub()

    for each event In m._pendingVideoEvents
      m._trackPlayerEvent(event["eventName"], event["contentID"], event["dimensions"], event["metrics"], event["startEvent"])
    next

    m._pendingVideoEvents = []

  end sub

  ' Track player event
  ' @param {*} eventName
  ' @param {*} contentID
  ' @param {*} dimensions
  ' @param {*} metrics
  ' @param {*} startEvent

  this._trackPlayerEvent = sub(eventName as string, contentID, dimensions, metrics, startEvent as boolean)
    ' Prepare dimensions

    dimensionsInternal = {}

    if (contentID <> invalid)
      dimensionsInternal["contentId"] = contentID
    end if

    ' Fire event

    m._fireEventVideo(eventName, dimensionsInternal, dimensions, metrics)

    ' Transition state to active

    if ( startEvent = false )
      m._setActive(eventName)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' CONTENT SEARCH
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Tracks search query events.
  ' @param {string} searchQuery The search term entered by the user.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackSearchQuery = sub(searchQuery, dimensions, metrics)

    if (NOT m._isString(searchQuery) OR searchQuery = "")
      ProductAnalyticsLog("Cannot track search query since no searchQuery has been supplied.")
    else if (m._checkState("track search query"))
      m._searchQuery = searchQuery
      m._fireEvent("Search Query", m._eventTypes.search, {
        "query": m._searchQuery
      }, dimensions, metrics)
    end if

  end sub

  ' Tracks search result events.
  ' @param {integer} resultCount The number of search results returned by a search query.
  ' @param {String} [searchQuery] The search term entered by the user.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackSearchResult = sub(resultCount, searchQuery, dimensions, metrics)

    if (NOT m._isInteger(resultCount) OR resultCount < 0)
      ProductAnalyticsLog("Cannot track search result since resultCount is invalid.")
    else if (m._checkState("track search result"))

      if (NOT m._isString(searchQuery) OR searchQuery = "")
        query = m._searchQuery
      else
        query = searchQuery
      end if

      m._fireEvent("Search Results", m._eventTypes.search, {
        "query": query,
        "resultCount": resultCount
      }, dimensions, metrics)

    end if

  end sub

  ' Tracks user interactions with search results.
  ' @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
  ' @param {integer} sectionOrder The section order within the page.
  ' @param {integer} column The content placement column. It is commonly used to indicate the column number where content is placed in a grid layout (i.e.1, 2, etc..).
  ' @param {integer} row The content placement row. It is commonly used to indicate the row number where content is placed in a grid layout (i.e.1, 2, etc..).
  ' @param {string} contentID The content identifier. It is used for internal content unequivocally identification (i.e., AAA000111222).
  ' @param {String} [searchQuery] The search term entered by the user.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackSearchClick = sub(section, sectionOrder, column, row, contentID, searchQuery, dimensions, metrics)

    if (NOT m._isInteger(column) OR column < 1)
      ProductAnalyticsLog("Cannot track search click since column is invalid.")
    else if (NOT m._isInteger(row) OR row < 1)
      ProductAnalyticsLog("Cannot track search click since row is invalid.")
    else if (NOT m._isString(contentID) OR contentID = "")
      ProductAnalyticsLog("Cannot track search click since no contentID has been supplied.")
    else if (m._checkState("track section click"))

      if (NOT m._isString(searchQuery) OR searchQuery = "")
        query = m._searchQuery
      else
        query = searchQuery
      end if

      if (NOT m._isString(section) OR section = "")
        section = "Search"
      end if

      if (NOT m._isInteger(sectionOrder) OR sectionOrder < 1)
        sectionOrder = 1
      end if

      m._fireEvent("Search Result Click", m._eventTypes.search, {
        "sectionOrder": sectionOrder,
        "section": section,
        "query": query,
        "column": column,
        "row": row,
        "contentId": contentID
      }, dimensions, metrics)

    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' EXTERNAL APPLICATIONS
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Tracks external app start events.
  ' @param {string} appName The name of the application being used to deliver the content to the end-user (i.e., Netflix).
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackExternalAppLaunch = sub(appName, dimensions, metrics)

    if (NOT m._isString(appName) OR appName = "")
      ProductAnalyticsLog("Cannot track external application launch since no appName has been supplied.")
    else if (m._checkState("track external application launch"))
      m._fireEvent("External Application Launch", m._eventTypes.externalApplication, {
        "paExtAppName": appName
      }, dimensions, metrics)
    end if

  end sub

  ' Tracks external app stop events.
  ' @param {string} appName The name of the application being used to deliver the content to the end-user (i.e., Netflix).
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackExternalAppExit = sub(appName, dimensions, metrics)

    if (NOT m._isString(appName) OR appName = "")
      ProductAnalyticsLog("Cannot track external application exit since no appName has been supplied.")
    else if (m._checkState("track external application exit"))
      m._fireEvent("External Application Exit", m._eventTypes.externalApplication, {
        "paExtAppName": appName
      }, dimensions, metrics)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' ENGAGEMENT
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Tracks engagement events.
  ' @param {string} eventName The name of the engagement event (i.e., Share, Save, Rate, etc.).
  ' @param {string} contentID The unique content identifier of the content the user is engaging with.
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackEngagementEvent = sub(eventName, contentID, dimensions, metrics)

    if (NOT m._isString(eventName) OR eventName = "")
      ProductAnalyticsLog("Cannot track engagement event since no eventName has been supplied.")
    else if (NOT m._isString(contentID) OR contentID = "")
      ProductAnalyticsLog("Cannot track engagement event since no contentID has been supplied.")
    else if (m._checkState("track engagement event"))
      m._fireEvent("Engagement " + eventName, m._eventTypes.engagement, {
        "contentId": contentID
      }, dimensions, metrics)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' CUSTOM EVENT
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Track custom event
  ' @param {string} eventName Name of the event to track
  ' @param {Object} [dimensions] Dimensions to track
  ' @param {Object} [metrics] Metrics to track

  this.trackEvent = sub(eventName, dimensions, metrics)

    if (NOT m._isString(eventName) OR eventName = "")
      ProductAnalyticsLog("Cannot track custom event since no eventName has been supplied.")
    else if (m._checkState("track custom event"))
      m._fireEvent("Custom " + eventName, m._eventTypes.custom, {
      }, dimensions, metrics)
    end if

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' STATE MANAGEMENT
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Start tracking user state

  this._userStateStart = sub()

    if m._userStateEnabled()
      m._storeUserState(m._userStates.passive)
    end if
    
  end sub

  ' Stop tracking user state

  this._userStateStop = sub()

    if m._userStateEnabled()
      m._userStateTimeout.control = "stop"
      m._storeUserState(invalid)    ' By setting _userState to invalid we effectively get it removed from options
    end if

  end sub

  ' Set active state
  ' @param {string} eventName
  ' @param {boolean} playerStarted

  this._setActive = sub(eventName as string)

    if m._userStateEnabled()

      state = m._userStates.active

      if (m._userState <> state)
        m._fireEventState(state, eventName)
        m._storeUserState(state)
      end if

      m._userStateTimeout.control = "stop"
      m._userStateTimeout.control = "start"

    end if

  end sub

  ' Set user state as passive after a timeout

  this.setPassive = sub()

    if m._userStateEnabled()

      state = m._userStates.passive

      m._fireEventState(state, "timer")
      m._storeUserState(state)

    end if

  end sub

  ' Fire switch state event
  ' @private

  this._fireEventState = sub(state as string, eventName as string)

    ProductAnalyticsLog("User changing from state " + m._userState + " to " + state)

    dimensions = {
      "newState": state,
      "triggerEvent": eventName,
      "stateFromTo": m._userState + " to " + state
    }

    ' Fire the event

    if ( state = m._userStates.active )
      m._fireEventVideo("Content Playback State Switch to Active", dimensions, invalid, invalid)
    else if ( state = m._userStates.passive )
      m._fireEventVideo("Content Playback State Switch to Passive", dimensions, invalid, invalid)
    end if

  end sub

  ' Store state
  ' @param {string} state
  ' @private

  this._storeUserState = sub(state)

    m._userState = state

    if m._plugin = invalid OR m._plugin.infoManager = invalid OR m._plugin.infoManager.options = invalid
     
      ProductAnalyticsLog("Cannot track user state since plugin options are unavailable.")

    else

      ' We are not supplying _userState as an option, because this option is retrieved in the setOptions 
      ' YBPluginGeneric callback.

      m._setOptions({})

    end if

  end sub

  ' Checks whether user state is enabled or not

  this._userStateEnabled = function() as boolean

    return ( m._initialized AND m._productAnalyticsSettings.enableStateTracking )
    
  end function

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' OPTION MANAGEMENT
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Get options

  this.getOptions = function() as object

    options = {}

    if ( m._userStateEnabled() )
      options[m._userStateDimension] = m._userState
    end if

    return options

  end function  

  ' Set options

  this._setOptions = sub(optionsNew)

    ' Get current options

    optionsOld = m._plugin.top.getField("options")

    if ( optionsOld = invalid )
      optionsOld = {}
    end if

    ' Ensure new options are valid

    if ( optionsNew = invalid )
      optionsNew = {}
    end if

    ' Populate new array

    options = {}

    for each key in optionsOld
      options[key] = optionsOld[key]
    end for

    for each key in optionsNew
      options[key] = optionsNew[key]
    end for

    m._plugin.top.setField("options", options)

    ' In addition to setting options to plugin, we also need setting options directly to infoManager
    ' since sometimes InfoManager_getRequestParams is called BEFORE plugin options callback and this
    ' may lead to an incorrect parameter communication.

    m._plugin.infoManager.options = options

  end sub

  ' Delete options

  this._deleteOptions = sub(optionsDelete)

    optionsOld = m._plugin.top.getField("options")

    if ( optionsOld = invalid )
      optionsOld = {}
    end if

    if ( optionsDelete = invalid )
      optionsDelete = []
    end if

    options = {}
    options.Append(optionsOld)

    for each option in optionsDelete
      options.Delete(option)
    end for

    m._plugin.top.setField("options", options)

    ' In addition to setting options to plugin, we also need setting options directly to infoManager
    ' since sometimes InfoManager_getRequestParams is called BEFORE plugin options callback and this
    ' may lead to an incorrect parameter communication.

    m._plugin.infoManager.options = options

  end sub

  ' ------------------------------------------------------------------------------------------------------------------------------
  ' INTERNAL
  ' ------------------------------------------------------------------------------------------------------------------------------

  ' Fires an event
  ' @param {string} eventName Name of the event to be fired
  ' @param {Object} dimensionsInternal Dimensions supplied by user
  ' @param {Object} dimensionsUser Specific event dimensions
  ' @param {Object} metrics Metrics to track
  ' @private

  this._fireEvent = sub(eventName as string, eventType as string, dimensionsInternal, dimensionsUser, metrics)

    ProductAnalyticsLog(eventName)

    ' Track event

    if (m._plugin = invalid)

      ProductAnalyticsLog("Cannot fire " + eventName + " since plugin is unavailable.")
     
    else

      ' Extract top level dimensions from custom dimensions

      dimensions = m._buildDimensions(eventType, dimensionsInternal, dimensionsUser)

      params = {
          "name": eventName,
          "dimensions": dimensions["custom"],
          "values": metrics
        }

      params.Append(dimensions["top"])

      m._plugin.eventHandler("sessionEvent", params)

    end if

  end sub

  ' Fires a video event (in case it is available)
  ' @param {string} eventName Event name
  ' @param {Object} dimensionsInternal Dimensions supplied by user
  ' @param {Object} dimensionsUser Specific event dimensions
  ' @param {Object} metrics Metrics to track
  ' @private

  this._fireEventVideo = sub(eventName as string, dimensionsInternal as object, dimensionsUser as object, metrics as object)

    ProductAnalyticsLog(eventName)

    if ( m._plugin = invalid )

      ProductAnalyticsLog("Cannot fire " + eventName + " video event since plugin is unavailable.")

    else

      dimensions = m._buildDimensions(m._eventTypes.contentPlayback, dimensionsInternal, dimensionsUser)

      params = { 
          "name": eventName,
          "dimensions": dimensions.custom, 
          "values": metrics
        }
      params.Append(dimensions.top)

      m._plugin.eventHandler("event", params)

    end if

  end sub

  ' Builds a list of top level and custom dimensions
  ' @param {String} eventType Object containing list of internal dimensions
  ' @param {Object} dimensionsInternal Object containing list of internal dimensions
  ' @param {Object} dimensionsUser Object containing list of custom dimensions
  ' @private

  this._buildDimensions = function(eventType as string, dimensionsInternal as object, dimensionsUser as object)

    ' Build custom event dimensions

    dimensionsCustom = {"paPage": m._page}

    if ( dimensionsInternal <> invalid )
      dimensionsCustom.Append(dimensionsInternal)
    end if

    if ( dimensionsUser <> invalid )
      dimensionsCustom.Append(dimensionsUser)
    end if

    dimensionsCustom.Append({"eventSource": "Product Analytics", "eventType": eventType})

    ' List of Top Level Dimension keys

    dimensionsTopLevelKeys = ["contentid", "contentId", "contentID", "utmSource", "utmMedium", "utmCampaign", "utmTerm", "utmContent", "profileId", "profile_id", "username"]

    ' Create object with top level dimensions

    dimensionsTopLevel = {}

    for each key in dimensionsCustom
      if (m._contained(dimensionsTopLevelKeys, key))
        dimensionsTopLevel[key] = dimensionsCustom[key]
      end if
    next

    ' Remove top level dimensions from custom dimensions list

    for each key in dimensionsTopLevelKeys
      dimensionsCustom.Delete(key)
    next

    return {"custom": dimensionsCustom, "top": dimensionsTopLevel}

  end function

  ' Check state before sending an event
  ' @param {String} message Message to show in case validation fails
  ' @private

  this._checkState = function (message) As boolean

    if m._initialized = false
      valid = false
      ProductAnalyticsLog("Cannot " + message + " since Product Analytics is uninitialized.")
    else if m._plugin = invalid
      valid = false
      ProductAnalyticsLog("Cannot " + message + " since plugin is unavailable.")    
    else if (m._plugin.sessionStarted = false)
      valid = false
      ProductAnalyticsLog("Cannot " + message + " since session is closed.")
    else
      valid = true
    end if

    return valid

  end function

  ' String is contained in array

  this._contained = function(arr as Object, str as String) as Boolean

    For Each item In arr
        If item = str Then 
            Return True
        End If
    End For
    Return False

  end function

  ' Variable is of type string?

  this._isString = function(variable) as Boolean

    Return (Type(variable) = "roString" OR Type(variable) = "String")

  end function

  ' Variable is of type integer?

  this._isInteger = function(variable) as Boolean

    Return (Type(variable) = "roInt" OR Type(variable) = "Integer")

  end function

  ' Variable is of type boolean?

  this._isBoolean = function(variable) as Boolean

    Return (Type(variable) = "roBoolean" OR Type(variable) = "Boolean")

  end function
 
  return this

end function

function ProductAnalyticsLog(message)

  YouboraLog(message, "Product Analytics")

end function
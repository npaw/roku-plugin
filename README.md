# Youbora Roku plugin

To build the sample application, execute `makeroku.sh`

## Folder Structure
```
/
└── RokuAdsPlugin/      Sourcecode of the project.
    └── components/
        └── youbora/    Sourcecode of the plugin.
```

## Documentation, Installation & Usage
Please refer to [this](http://developer.nicepeopleatwork.com/plugins/integration/) developer's portal post.

## I need help!
If you find a bug, have a suggestion or need assistance send an e-mail to <support@nicepeopleatwork.com>

sub init()
	CommunicationLog("Created Communication")

  ' Fields

  m._port      = CreateObject("roMessagePort")
  m._sessions  = CreateObject("roList")
  m._count     = 0

  m.top.setField("pingTime", 5)
 
  m.top.observeField("sessionStart", m._port)
  m.top.observeField("request",      m._port)
  m.top.observeField("nextView",     m._port)
  m.top.observeField("httpSecure",   m._port)
  m.top.observeField("close",        m._port)

  ' Get active session (the last one)
  
  m._getActiveSession = function()
  
    if ( m._sessions.isEmpty() )
      session = invalid
    else
      session = m._sessions.GetTail()
    end if
  
    return session
  
  end function

  ' Send next session request

  m._sendNextSessionRequest = sub()

    ' Do we have a new session to send?

    if ( m._sessions.Count() > 1 )

      ' Get current session

      session = m._sessions.GetHead()

      ' If the current session has no further requests to send...

      if ( session.requestsQueueIsEmpty()  )

        ' ... delete it
  
        m._sessions.RemoveHead()
        
        ' ... and send the FastData request of the next session
  
        session = m._sessions.GetHead()
        session.dataRequestSend()

      end if

    end if
    
  end sub

  ' Init ourselves
 
  m.top.functionName = "_run"
  m.top.control      = "RUN"

end sub

' Main loop

sub _run()

  m.httpSecure = m.top.httpSecure

  'Endless loop to listen for events

  while true

    msg = wait(0, m._port)
    mt  = type(msg)

    if mt = "roSGNodeEvent"

      if msg.getField() = "sessionStart"

        ' Add FastData request

        m._count += 1

        session = YBSession(m._port, m._count)
        m._sessions.Push(session)
        session.dataRequestPrepare(msg.getData())

        if ( m._sessions.Count() = 1 )
          session.dataRequestSend()
        end if

      else if msg.getField() = "request"

        ' Add request (to the last session)

        session = m._getActiveSession()

        if ( session = invalid )
          CommunicationLog("Cannot send request since no session has been started " + msg.getData().service)
        else
          reqInfo = msg.getData()
          service = reqInfo.service
          args    = reqInfo.args
          session.requestSend(service, args)
        end if

      else if msg.getField() = "nextView"

        session = m._getActiveSession()

        if ( session = invalid )
          CommunicationLog("Cannot increase view since no session has been started")
        else
          session.nextView()
        end if

      else if msg.getField() = "httpSecure"

        session = m._getActiveSession()

        if ( session = invalid )
          CommunicationLog("Cannot change protocol since no session has been started")
        else
          session.setRequestProtocol(msg.getData())
        end if

      else if msg.getField() = "close"

        exit while

      endif   

    else if mt = "roUrlEvent"

      ' Process request response:
      '   We are processing sessions sequentially, that is, we proceed to the next session
      '   once all the previous session requests have been sent. This is why we are only 
      '   monitoring the first session from the array. 

      if ( m._sessions.Count() > 0 )

        session = m._sessions.getHead()
        info    = session.processResponse(msg)

        if info["processed"] = false

          ' The request does not belongs to this session

          CommunicationLog("Error: request not found when processing response")

        else

          ' The request belongs to this session and...

          if info["type"] = "request"

            ' ... is of type request: send next FastData request (if available) when there are no pending requests

            m._sendNextSessionRequest()

          else if info["type"] = "data"
            
            ' ... is of type data...
    
            if ( info["callback"] = "dataRequestReceived" )
    
              ' ... FastData response received: update ping time

              if ( session.pingTime <> invalid )
                m.top.setField("pingTime", session.pingTime)
              end if
    
              ' ... send next FastData request in case the current session has no requests in it

              m._sendNextSessionRequest()

            else

              ' ... fastData response error: retry

              session.dataRequestSend()

            end if

          end if

        end if 
        
      end if

    end if

  end while

end sub

' Log message

sub CommunicationLog(message)
  YouboraLog(message, "Communication")
end sub
function YBSession(port, number) As Object

  this = CreateObject("roAssociativeArray")

  ' -------------------------------------------------------------------------------------------
  ' Attribute definition
  ' -------------------------------------------------------------------------------------------
  
  this.port                  = port
  this.code                  = invalid
  this.pingTime              = invalid
  this.youboraId             = invalid

  this._number               = number
  this._view                 = -1
  this._requestSecure        = true
  this._requestHost          = "a-fds.youborafds01.com"
  this._requestDataArgs      = invalid
  this._requestData          = invalid
  this._requestDataProcessed = false
  this._requests             = {}
  this._requestsProcessing   = CreateObject("roList")

  ' -------------------------------------------------------------------------------------------
  ' Method definition - Session Handling
  ' -------------------------------------------------------------------------------------------

  ' Prepare FastData request

  this.dataRequestPrepare = sub(args = invalid)

    ' Store arguments in case we need to retry the request

    m._requestDataArgs = args

    ' Validate arguments

    if args = invalid
      args = {}
    end if

    args.Delete("code")

    if args.DoesExist("outputformat") = false 
      args["outputformat"] = "jsonp"
    end if

    args["timemark"] = currentMillis().ToStr()

    m._log("FastData: preparing request")

    m._requestData         = Request(m.port)
    m._requestData.host    = m._buildHost()
    m._requestData.service = "/data"
    m._requestData.args    = args

  end sub

  ' Send FastData request

  this.dataRequestSend = sub()

    m._log("FastData: sending request")

    if m._requestData = invalid
      m._log("    Cannot send data request since it is unavailable")
    else
      m._requestData.send()
    end if

  end sub

  ' FastData response received

  this._dataReceived = function(msg)

    m._log("FastData: response received")

    callback = invalid

    m.code         = invalid
    m.pingTime     = invalid
    m.youboraId    = invalid
    m._requestHost = invalid

    responseCode = msg.GetResponseCode()

    if (responseCode <> 200)

      m._log("    unsuccessful: " + responseCode.ToStr() + ", reason: " + msg.GetFailureReason())

    else

      response = msg.GetString()

      if response = invalid OR response = ""  OR Len(response) < 8
        response      = invalid
        responseJson  = invalid
        outerJson     = invalid
      else
        'Convert jsonp to json
     
        responseJson = mid(response, 8, Len(response) - 8)
        outerJson    = ParseJSON(responseJson)
      end if
  
      if response = invalid
        m._log("    empty response")
      else if outerJson = invalid
        m._log("    outer JSON invalid")
      else if outerJson.q = invalid
        m._log("    q invalid")
      else
        innerJson = outerJson.q
        host      = ""
        code      = ""
        pt        = ""
        yid       = ""

        if innerJson.h <> invalid
          host = innerJson.h
        end if

        if innerJson.c <> invalid
          code = innerJson.c
        end if

        if innerJson.pt <> invalid
          pt = innerJson.pt
        end if

        if innerJson.f <> invalid
          yid = innerJson.f.yid
        end if

        if Len(host) > 0 and Len(code) > 0 and Len(pt) > 0

          pt = parseNumber(pt)

          m.code         = code
          m.pingTime     = pt
          m.youboraId    = yid
          m._requestHost = host
          m._requestDataProcessed = true
          callback = "dataRequestReceived"

          m._log("    session " + code + " is ready.")

        end if

      end if

    end if

    m._requestData = invalid

    if ( m.code = invalid )
     
      ' FastData request has failed: retry

      m._requestData(m._requestDataArgs)

    else

      ' FastData request has succeeded: process requests

      m._processRequests()
      
    end if

    return callback

  end function

  ' -------------------------------------------------------------------------------------------
  ' Method definition - Request Handling
  ' -------------------------------------------------------------------------------------------

  ' Add request to the send queue
 
  this.requestSend = sub(service, args = Invalid)

    m._log("Preparing request: " + service)

    if args = Invalid
      args = {}
    end if

    ' Remove code from argument list

    args.delete("code")

    ' Get view code and add it to the request list

    viewCode = m._getViewCode()
   
    if m._requests.DoesExist(viewCode) = false
      m._requests[viewCode] = []
    end if

    ' Check queue size before adding the new request
  
    currentQueueSize = m._queueSize(viewCode)

    if currentQueueSize >= YouboraConstants().QUEUE_LIMIT_SIZE
      m._log("[Warning] Request queue size for code " + viewCode + " has reached the limit (Queue size: " + currentQueueSize.ToStr() + ")")
      return
    end if

    ' Populate request and add it to the queue
  
    req               = Request(m.port)
    req.service       = service
    req.args          = args
    req.args.timemark = currentMillis().ToStr()
    m._requests[viewCode].Push(req)

    ' Process requests

    m._processRequests()
  
  end sub

  ' Process request queue

  this._processRequests = sub()
  
    if m._requestDataProcessed AND m._requests.IsEmpty() = false

      ' Iterate over requests's keys, ie. the view codes

      for each viewCode in m._requests 

        requestsForCode = m._requests[viewCode]
  
        ' Iterate pending requests for current view code

        for each req in requestsForCode 
  
          req.args.code           = viewCode
          req.args["sessionRoot"] = m.code

          if req.host = Invalid or req.host = ""
            req.host = m._buildHost()
          end if
  
          if req.service = "/start" OR req.service = "/init"
            req.args["youboraId"] = m.youboraId
          end if
  
          if req.service = "/start" OR req.service = "/init" OR req.service = "/ping" OR req.service = "/error"
            req.args["sessionParent"] = m.code 'Since we won't have parent we use the same code as root
          end if
  
          if req.service = "/start" OR req.service = "/init" OR req.service = "/error"
            req.args["parentId"] = req.args["sessionRoot"]
          end if
  
          if (req.service = "/infinity/session/start" OR req.service = "/infinity/session/stop" OR req.service = "/infinity/session/nav" OR req.service = "/infinity/session/event" OR req.service = "/infinity/session/beat")

            req.args.Delete("code")
            req.args["sessionId"] = m.code
  
            if req.service = "/infinity/session/start"
              m.sessionStarted = true
            end if
  
            if req.service = "/infinity/session/stop"
              m.sessionStarted = false
            end if

          end if

          ' Send request
  
          m._log("Sending request: " + req.service)
          req.send()

          ' Add request to the list of requests being processed
  
          m._requestsProcessing.Push(req)

        end for

      end for
  
      ' Clear request list once sent
      
      m._requests.Clear()

    end if

  end sub

  ' Process request response

  this._requestReceived = function(msg, req, reqIndex)

    code = msg.GetResponseCode()

    if code = 200
      m._log("    " + req.service +  " response received successfully.")
    else
      m._log("    " + req.service +  " response received with error. Code: " + code.ToStr() + ", reason: " + msg.GetFailureReason())
    end if

    ' Remove request from the list of requests pending of being processed

    if ( reqIndex <> invalid AND reqIndex >= 0 AND reqIndex < m._requestsProcessing.Count() )
      m._requestsProcessing.Delete(reqIndex)
    else
      m._log("    cannot remove request from list since index is not valid")
    end if

    return invalid

  end function

  ' -------------------------------------------------------------------------------------------
  ' Method definition - Response handling
  ' -------------------------------------------------------------------------------------------

  ' Find request by request identity

  this._findRequest = function(requestIdentity as integer)

    info = {found: false, type: invalid, request: invalid, requestIndex: invalid}

    if ( m._requestData <> invalid AND m._requestData.request.GetIdentity() = requestIdentity )

      info["found"]   = true
      info["type"]    = "data"
      info["request"] = m._requestData

    else

      for i = 0 to m._requestsProcessing.Count() - 1

        req = m._requestsProcessing[i]

        if requestIdentity = req.request.GetIdentity()
          info["found"]        = true
          info["type"]         = "request"
          info["request"]      = req
          info["requestIndex"] = i
        end if

      end for
      
    end if

    return info

  end function

  ' Process response

  this.processResponse = function(msg)

    processed = false

    info = m._findRequest(msg.GetSourceIdentity())

    if info["found"] = false
      callback = invalid
    else if info["type"] = "data"
      callback  = m._dataReceived(msg)
    else
      callback  = m._requestReceived(msg, info["request"], info["requestIndex"])
    end if

    return {processed: info["found"], type: info["type"], callback: callback}

  end function

  ' -------------------------------------------------------------------------------------------
  ' Method definition - Queue handling
  ' -------------------------------------------------------------------------------------------

  ' Get request queue size

  this._queueSize = function(viewCode as String)
   
    try
     
      reqs = m._requests
  
      if reqs = invalid or viewCode = invalid
        return 0
      end if
  
      if reqs.DoesExist(viewCode) = false
        return 0
      else
        return reqs[viewCode].count()
      end if

    catch e

      return 0

    end try

  end function

  ' Check if requests queue is empty

  this.requestsQueueIsEmpty = function() as boolean

    return (m._requests.isEmpty() AND m._requestsProcessing.isEmpty())

  end function

  ' -------------------------------------------------------------------------------------------
  ' Method definition - View code handling
  ' -------------------------------------------------------------------------------------------

  ' Increase view counter

  this.nextView = function() as String

    m._view = m._view + 1
    return m._getViewCode()

  end function

  ' Retrieve view code
  
  this._getViewCode = function() as String
   
    if m.code = Invalid or m.code = ""
      viewCode = "nocode"
    else
      viewCode = m.code + "_" + m._view.ToStr()
    end if

    return viewCode
    
  end function

  ' -------------------------------------------------------------------------------------------
  ' Method definition - Protocol handling
  ' -------------------------------------------------------------------------------------------

  ' Set request protocol

  this.setRequestProtocol = sub(secure as boolean)

    m._requestSecure = secure

    ' Update data request host

    if ( m._requestData <> invalid )
      m._requestData = m._buildHost()
    end if

    ' Update requests host

    if ( m._requestDataProcessed )

      for each viewCode in m._requests

        for each req in m._requests[viewCode]
  
          if req.host = Invalid or req.host = ""
            req.host = m._buildHost()
          end if
  
        end for

      end for

    end if

  end sub

  ' -------------------------------------------------------------------------------------------
  ' Method definition - Helpers
  ' -------------------------------------------------------------------------------------------

  ' Build host for requests

  this._buildHost = function() as String

    if m._requestSecure = true
      proto = "https://"
    else
      proto = "http://"
    end if
  
    return proto + m._requestHost
  
  end function

  ' Log message

  this._log = sub(message)
    YouboraLog("[" + m._number.ToStr() + "] " + message, "YBSession")
  end sub

  return this

end function


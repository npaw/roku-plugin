## [6.6.17] - 2025-03-10
### Added
[PROD-1123]
- Product Analytics: add `paReferrer` dimension to navigation events.
### Modified
[PROD-1123]
- Product Analytics: rename `page` dimension as `paPage`.
- Product Analytics: event name `Navigation {pageName}` is renamed as `Navigation`.
- Product Analytics: navigation dimensions `route`, `routeDomain` and `fullRoute` are renamed as `paRoute`, `paRouteDomain` and `paFullRoute`.
- Product Analytics: attribution dimension `url` is renamed as `utmUrl`.
- Product Analytics: attribution UTM dimensions are sent only as TLD.
- Product Analytics: external application dimension `appName` is renamed as `paExtAppName`.

## [6.6.16] - 2024-10-23
### Added
[PROD-996]
- Product Analytics: added `loginSuccessful`, `loginUnsuccessful` and `logout` methods. Also added `userProfileCreated`, `userProfileSelected` and `userProfileDeleted`.
- `profileId` is sent on `infinity/session/start`, `infinity/session/nav`, `/init`, `/start` and `/error` events (the same events where `username` is included).
### Modified
- Product Analytics: `trackNavByName` no longer start a session or send a session/nav event.
- Product Analytics: initialize starts a session.
- Product Analytics: tracking methods fail when called within a closed session.

## [6.6.15] - 2024-10-22
### Fixed
- [PROD-1035] `Communication` holds an array of `YBSession`. Every `YBSession` has a `FastData` request and a list "Session" requests. Requests are served sequentially, that is, no "Session" requests are sent until `FastData` response is received. The `YBSession` is removed once there are no further requests to serve and a newer `YBSession` is available.

## [6.6.14] - 2024-08-27
### Added
- Initial version of Product Analytics.
### Fixed
- Add missing entities to be sent upon change.
- Handle ping and beat callbacks properly so that no beats are sent when session is stopped and video is still playing.
- Set screen name param name as `page` instead of `screeName`.
- Replace `print` statements with `YouboraLog`.

## [6.6.13] - 2024-08-14
### Fixed
- Fix bug by forcing flag isFinished to be false on `init`, `join` and `play`.

## [6.6.12] - 2024-05-22
### Fixed
- Remove unused variables

## [6.6.11] - 2023-09-26
### Added
- Re-added the Content Package to the options.

## [6.6.10] - 2023-08-18
### Fixed
- Get adNumber value from YB component var instead of ads object, to avoid send same adNumber value in different ad blocks (for same adPosition)
- Adding support to disable detect seeks events on playhead jumps by option ["playheadJumpEnabled"]

## [6.6.9] - 2023-08-18
### Fixed
- Reset adBreakNumber value if adPosition has changed

## [6.6.8] - 2023-08-16
### Fixed
- Check playhead value to detect if player buffer event is a seek if was not paused previously
- Adding support to define seek event is not enabled by options ["seekEventEnabled"] (for some live views)

## [6.6.7] - 2023-08-08
### Fixed
- Adding updatePlayer method to add possibility to update new player object

## [6.6.6] - 2023-07-27
### Fixed
- Use HdmiStatus object instead of deprecated IsHDMIConnected DeviceInfo method

## [6.6.5] - 2023-06-27
### Fixed
- If PodStart/PodStop event has rendersequence defined, but ads event doesn't have it, get it from stored adPosition (on PodStart event)

## [6.6.4] - 2023-06-14
### Added
- Adding memory percent KPI on ping and beat events
- Adding new custom dimensions for each new view: hdmiConnected, connectionType, displayType and videoMode
- Adding support for new session error event
### Fixed
- Fix issue with invalid adNumberInBreak value in some scenarios
- Capture errors on some YBPluginRokuVideo component if videoplayer object is not defined 

## [6.6.3] - 2023-06-02
### Fixed
- If buffer is triggered after seek event, ignore buffer event because is on seek
- On getRendition method, ignore captions segment type (to avoid inform invalid rendition values)
- On getBitrate method, try to get it from current streamingSegment object, but, if is not valid, get if from last valid streamingSegment

## [6.6.2] - 2023-05-16
### Fixed
- On eventHandler subroutine, check if params is invalid for error events, to avoid possible issues

## [6.6.1] - 2023-04-25
### Fixed
- Remove invalid error print message, adding error handling on some blocks (YouboraLog and some methods on YBPluginRokuVideo component)

## [6.6.0] - 2023-04-18
### Fixed
- Simplify, on view code generation, use prefix to define view type

## [6.5.33] - 2023-04-18
### Fixed
- If FastData did not provide a valid response, do not enqueue requests
- Adding constants to define queueSize as constant, and don't see to the queue if the plugin reached the limit

## [6.5.32] - 2023-04-18
### Added
- Added component and datetime info on youbora logs

## [6.5.31] - 2023-03-17
### Fixed
- On getBitrate method, fix to not send captions bitrate

## [6.5.30] - 2023-01-18
### Added
- `user.privacyProtocol` option

## [6.5.29] - 2022-07-12
### Added
- `user.privacyProtocol` option

## [6.5.28] - 2022-06-01
### Fixed
- Buffer detection with player state changes
- Missing jointime when having prerolls
- Wrong reporting of adnumber and adnumberinbreak

## [6.5.27] - 2022-05-06
### Added
- `device.id` option to modify default value reported in `deviceUUID`

## [6.5.26] - 2022-04-11
### Added
- Device detection (device code, device name) for modern devices without adding them in a dictionary

## [6.5.25] - 2022-04-07
### Added
- `deviceUUID` parameter for video and sessions start, automatically tracked

## [6.5.24] - 2022-02-23
### Added
- New `entities` implementation including more parameters for ping event
- Metrics reported outside entities in pings

## [6.5.23] - 2022-02-14
### Added
- `playrate` and `diffTime` parameters in pings
- Check to avoid url not being cleared between views

## [6.5.22] - 2021-12-22
### Removed
- `content.package` option
### Addeed
- `adnumberinbreak` parameter for ads

## [6.5.21] - 2021-10-21
### Fixed
- Performance issues with access to global youboraLog boolean

## [6.5.20] - 2021-09-20
### Added
- `content.customDimensions` option, as an alternative way to use custom dimensions like `content.customDimension.1`

## [6.5.19] - 2021-09-02
### Fixed
- Skipped audio chunks for bitrate and rendition detection

## [6.5.18] - 2021-08-09
### Added
- Width and height of the video added to rendition when available

## [6.5.17] - 2021-07-07
### Added
- `obfuscateIp` for sessions
- Device detection rework to track device code

## [6.5.16] - 2021-06-22
### Added
- New devices codes

## [6.5.15] - 2021-04-22
### Added
- `network.connectionType` option (thanks Tim Lacey)
- Deprecated deviceInfo.getVersion() replaced with deviceInfo.GetOSVersion() (thanks Tim Lacey)

## [6.5.14] - 2021-03-31
### Added
- Support for `CMF` transport format

## [6.5.13] - 2021-02-26
### Fixed
- Init being sent after start breaking the logic of the views.

## [6.5.12] - 2021-02-09
### Removed
- Logs by default, now all are hidden

## [6.5.11] - 2020-12-29
### Added
- Additional parameters for session start request

## [6.5.10] - 2020-08-20
### Fixed
- Additional check for wrong fastdata responses

## [6.5.9] - 2020-06-16
### Added
- `content.transportFormat` and `content.streamingProtocol` options and getters from plugin
- Option `content.subtitles` reported in start and pings, as entities
- Totalbytes feature: `content.sendTotalBytes`and `content.totalBytes` options, and getTotalBytes in plugin
### Fixed
- Option `user.type`

## [6.5.8] - 2020-06-05
### Fixed
- Finished flag not resetting properly when task stopped

## [6.5.7] - 2020-05-20
### Added
- `parse.manifest` option

## [6.5.6] - 2020-05-08
### Fixed
- Removed beat stop when video view ends
- Player was not 'resetting' in case of changing it

## [6.5.5] - 2020-02-10
### Added
- adManifest event
- `ad.creativeId`, `ad.breaksTime`, `ad.expectedPattern`, `ad.provider`, `ad.givenAds`, `ad.givenBreaks` options

## [6.5.4] - 2020-01-28
### Fixed
- Player being registered only once
### Added
- ErrorMetadata parameter in errors
### Removed
- Mediaduration from jointime event

## [6.5.3] - 2019-12-24
### Fixed
- Negative join time in case of opening vie with /start request ( /init was working fine)

## [6.5.2] - 2019-11-26
### Fixed
- Infinity event names was always 'event' not can be customized using 'displayName' on the object

## [6.5.1] - 2019-10-15
### Added
- IMA DAI support

## [6.5.0] - 2019-09-25
### Added
- All new ad events (break start, break end, quartile, etc)
- Session events
- Custom video events

## [6.3.5] - 2019-09-10
### Added
- New flag that lets you know when the plugin is started

## [6.3.4] - 2019-06-13
### Fixed
- Various minor fixes

## [6.3.3] - 2019-05-22
### Improved
- Several changes have been made to reduce the number or rendevouz (about 43% improvement on a 120 seconds play)

## [6.3.2] - 2019-05-08
### Updated
- Fast data url has changed
### Fixed
- Buffer detection is back the way it used to be

## [6.3.1] - 2019-04-03
### Fixed
- Proper seek and buffer detection was reverted from previous versions, now is back

## [6.3.0] - 2019-04-01
### Added
- Now is possible to delay the start event (and therefore have all the metadata ready) and have correcto joinTime
- New options: `user.email`, `content.package`, `content.saga`, `content.tvShow`, `content.season`, `content.episodeTitle`, `content.channel`, `content.id`, `content.imdbId`, `content.gracenoteId`, `content.type`, `content.genre`, `content.language`, `content.subtitles`, `content.contractedResolution`, `content.cost`, `content.price`, `content.playbackType`, `content.drm`, `content.encoding.videoCodec`, `content.encoding.audioCodec`, `content.encoding.codecSettings`, `content.enconding.codecProfile`, `content.encoding.containerFormat`, `ad.customDimension.x`, `user.obfuscateIp` and `user.type`
- New option alias: `user.name`, `user.anonymousId` and `content.customdimension.x`instead of `customDimension.x`
- Now our Roku sdk makes the HDMI cable on roku optional, totally wireless
## [6.2.0] - 2019-03-27
### Added
- App dimensions as options
- Add fingerprint with youboraId
- Account code now send on every request
- DeviceInfo now can be overriden from the options
### Improved
- Buffer detection
- Init is send automatically now
- AdInit is send automatically now
### Fixed
- Flags not reseting properly on stop
### Deprecated
- Extraparams are rename to customDimension.N
- title2 now is named program

## [6.1.4] - 2019-02-19
### Fixed
- Flags are not resetting anymore unless they ware changed again

## [6.1.3] - 2019-01-03
### Fixed
- Correct join time for more than one ad
- Add /adError request

## [6.1.2] - 2018-11-12
### Fixed
- Now buffer and seek are not mixed together and are reported correctly

## [6.1.1] - 2018-11-07
### Added
- Add new option parameters. smartswitch.configcode, smartswitch.groupcode, smartswitch.contractcode
- Option to disable error reporting just in case retries are being used)
### Fixed
- Stop being omited when using init

## [6.1.0] - 2018-10-15
### Added
- Add /init request

## [6.0.4] - 2018-10-26
### Fixed
- Now join time is calculated properly even if there is more than one ad
- Add /adError request support

## [6.0.3] - 2018-10-15
### Fixed
- Add pauseDuration where it was missing

## [6.0.2] - 2018-10-08
### Fixed
- Fix unobserving events

## [6.0.1] - 2018-08-08
### Fixed
- Wrong key in data and start calls, replaced with "system"

## [6.0.0] - 2018-06-01
### Added
- Ad tracking

' ********** Copyright 2017 Roku Corp.  All Rights Reserved. **********
Library  "Roku_Ads.brs"
function init()
    m.top.functionName = "playContent"
    m.top.id = "PlayerTask"
    m.top.ObserveField("state", "_stateListener")
    m.youboraPlugin = m.global.YB_Plugin_Roku
end function

function playContent()
    playWithRAF = m.top.playWithRAF
    contentInfo = m.top.contentInfo

    m.global.YB_Plugin_Roku.productAnalytics = {
      method: "trackNavByName",
      page: "Player " + playWithRAF
    }

    if playWithRAF = "standard"
        PlayContentWithFullRAFIntegration(contentInfo)
    else if playWithRAF = "nonstandard"
        PlayContentWithNonStandardRAFIntegration(contentInfo)
    else if playWithRAF = "stitched"
        PlayStitchedContentWithAds(contentInfo)
    end if
end function

function setupLogObject(adIface as object)
    ' Create a log object to track events
    logObj = {
        Log: function(evtType = invalid as dynamic, ctx = invalid as dynamic)
            if GetInterface(evtType, "ifString") <> invalid
                ? "*** tracking event " + evtType + " fired."
                if ctx.errMsg <> invalid then ? "*****   Error message: " + ctx.errMsg
                if ctx.adIndex <> invalid then ? "*****  Ad Index: " + ctx.adIndex.ToStr()
                if ctx.ad <> invalid and ctx.ad.adTitle <> invalid then ? "*****  Ad Title: " + ctx.ad.adTitle
                'else if ctx <> invalid and ctx.time <> invalid
                '? "*** checking tracking events for ad progress: " + ctx.time.ToStr()
            end if
            processAdEvent(ctx)
        end function
    }
    ' Create a log function to track events
    logFunc = function(obj = invalid as dynamic, evtType = invalid as dynamic, ctx = invalid as dynamic)
        obj.log(evtType, ctx)
    end function
    ' Setup tracking events callback
    setLog = adIface.SetTrackingCallback(logFunc, logObj)
end function

function processAdEvent(ctx = invalid as dynamic)
    m.youboraPlugin.adevent = ParseJson(FormatJson(ctx))
end function
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' "standard"
'  A full RAF integration Example:
'
' - Include RAF.
' - setAdURL to set the ad URL.
' - Examples of RAF MACROS being passed in the ad call.
' - getAds() for VAST parsing.
' - showAds for rendering.
' - Enable ad measurement.
' - Pass all parameters to measurement beacons with examples of genre, program id and content.
'@paracontentInfo [AA] object that has valid data for playing video with roVideoScreen.
function PlayContentWithFullRAFIntegration(contentInfo as object)
    adIface = Roku_Ads() 'RAF initialize
    'adIface.setDebugOutput(true) 'for debug logging

    'Ad measurement content params
    adIface.enableAdMeasurements(true)
    adIface.setContentLength(contentInfo.length.ToInt())
    adIface.setContentId(contentInfo.contentId)
    adIface.setContentGenre(contentInfo.genre)

    'Indicates whether the default Roku backfill ad service URL
    'should be used in case the client-configured URL fails (2 retries)
    'to return any renderable ads.
    adIface.setAdPrefs(true, 2)

    ' Normally, would set publisher's ad URL here.
    ' Otherwise uses default Roku ad server (with single preroll placeholder ad)
    adIface.setAdUrl(contentInfo.adUrl)

    'Returns available ad pod(s) scheduled for rendering or invalid, if none are available.
    adPods = adIface.getAds()

    playVideoWithAds(adPods, adIface, "FullRafIntegration")
end function

function playVideoWithAds(adPods as object, adIface as object, title as String) as void
    m.top.facade.visible = false
    keepPlaying = true
    '
    ' render pre-roll ads
    '
    video = m.top.video
    ' `view` is the node under which RAF should display its UI (passed as 3rd argument of showAds())

    ' Enable Youbora logging

    m.global.addFields({YouboraLogActive: true})
    
    ' > Two methods to define player
    ' First method is set videoplayer reference
    'm.youboraPlugin.videoplayer = m.top.video 'Pass Video reference to Youbora
    
    ' Second method is set an array on updateplayer field
    m.youboraPlugin.updateplayer = {player: m.top.video, unobserveGlobalScope: true}
    
    ' Set plugin options

    m.youboraPlugin.options = GetOptions(title, 1, "2")
    
    'm.youboraPlugin.control = "RUN" 'Start monitoring
    setupLogObject(adIface)

    view = video.getParent()
    if adPods <> invalid and adPods.count() > 0 then
        ' pre-roll ads
        keepPlaying = adIface.showAds(adPods, invalid, view)
    end if
    if not keepPlaying then return

    port = createObject("roMessagePort")
    video.observeField("position", port)
    video.observeField("state", port)
    video.visible = true
    video.control = "play"
    video.setFocus(true)

    curPos = 0
    adPods = invalid
    isPlayingPostroll = false
    while keepPlaying
        msg = wait(0, port)
        msgType = type(msg)
        if msgType = "roSGNodeEvent"
            if msg.GetField() = "position" then
                'render mid-roll ads
                curPos = msg.getData()
                adPods = adIface.getAds(msg)
                if adPods <> invalid and adPods.count() > 0
                    ' ask the video to stop
                    video.control = "stop"
                    ' then the rest is handled by "stopped" branch below
                end if
            else if msg.GetField() = "state" then
                curState = msg.getData()
                if curState = "stopped" then
                    if adPods = invalid or adPods.count() = 0 then
                        exit while
                    end if
                    YouboraLog("PlayerTask: playing midroll/postroll ads", "PlayerTask")
                    options = m.youboraPlugin.options
                    options["content.title"] = "asd"
                    options["content.metrics"] = { "key" : "asd", "key2" : "value2"}
                    m.youboraPlugin.options = options
                    keepPlaying = adIface.showAds(adPods, invalid, view)
                    adPods = invalid
                    if isPlayingPostroll then
                        exit while
                    end if
                    if keepPlaying then
                        YouboraLog("PlayerTask: mid-roll finished, seek to " + stri(curPos), "PlayerTask")
                        video.visible = true
                        video.seek = curPos
                        video.control = "play"
                        video.setFocus(true) 'important: take the focus back (RAF took it above)
                    end if

                else if curState = "finished" then
                    YouboraLog("PlayerTask: main content finished", "PlayerTask")
                    'render post-roll ads
                    adPods = adIface.getAds(msg)
                    if adPods = invalid or adPods.count() = 0 then
                        exit while
                    end if
                    YouboraLog("PlayerTask: has postroll ads", "PlayerTask")
                    isPlayingPostroll = true
                    ' stop the video, the post-roll would show when the state changes to  "stopped" (above)
                    video.control = "stop"
                end if
            end if
        end if
    end while
end function

' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' "nonstandard"
' A RAF implementation for non-standard ad responses (neither VMAP, VAST or SMartXML)
'
' - Custom Parsing of ad response to create the ad structure.
' - importAds().
' - showAds() for rendering.
'
function PlayContentWithNonStandardRAFIntegration(contentInfo as object)
    ' configure RAF
    raf = Roku_Ads()            'init RAF library instance
    raf.setAdPrefs(false)       'disable back-filled ads
    'raf.setDebugOutput(true)    'debug console (port 8085) extra output ON

    setupLogObject(raf)

    ' import custom parsed ad pods array
    adPods = []
    if contentInfo <> invalid and contentInfo.nonStandardAdsFilePath <> invalid
        adPods = GetNonStandardAds(contentInfo.nonStandardAdsFilePath)
        raf.importAds(adPods)
    else
        adPods = raf.getAds()
    end if
    ' process preroll ads
    playVideoWithAds(adPods, raf, "NonStandardRafIntegration")
end function

' Gets and parses ad pods array from non-standard JSON feed stored in file
' @param filePath [String] path to the file containing JSON ads feed
' @return [Object] ad pods as roArray to import into RAF
function GetNonStandardAds(filePath as string) as object
    feed = ReadAsciiFile(filePath)
    result = ParseJson(feed)
    if type(result) <> "roArray"
        return []
    end if
    ' parse ad pods in array according to the format accepted by RAF adding missing fields/values
    for each adPod in result
        if adPod <> invalid and adPod.ads <> invalid
            adPod.duration = 0
            adPod.viewed = false

            for each ad in adPod.ads
                if ad <> invalid
                    if ad.adServer = invalid
                        ad.adServer = ""
                    end if

                    if ad.duration = invalid
                        ad.duration = 0
                    end if

                    if ad.duration > 0
                        adPod.duration = adPod.duration + ad.duration
                    end if

                    if type(ad.tracking) = "roArray"
                        for each adBeacon in ad.tracking
                            if adBeacon <> invalid
                                adBeacon.triggered = false
                            end if
                        end for
                    end if
                end if
            end for
        end if
    end for

    return result
end function


' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' "stitched"
' A stitched Ads integration Example:
'
' - setAdURL to set the ad URL.
' - getAds for VAST parsing.
' - stitchedAdsInit to import ad metadata for server-stitched ads.
' - stitchedAdHandledEvent to determines if a stitched ad is being rendered and returns
'   metadata about the ad, after handling the event.
function PlayStitchedContentWithAds(contentInfo as object)
    ' init RAF library instance
    adIface = Roku_Ads()
    ' for debug logging
    'adIface.setDebugOutput(true) ' set True to debug console (port 8085) extra output ON

    'Ad measurement content params
    adIface.enableAdMeasurements(true)
    adIface.setContentLength(contentInfo.length.ToInt())
    adIface.setContentId(contentInfo.contentId)
    adIface.setContentGenre(contentInfo.genre)

    setupLogObject(adIface)

    ' Get adPods array. Normally, we would set publisher's ad URL using setAdUrl and get adPods from getAds function.
    if contentInfo <> invalid and contentInfo.stitchedAdsFilePath <> invalid
        adPodArray = GetAdPods(contentInfo.stitchedAdsFilePath)
        ' Imports adPods array with server-stitched ads
        adIface.StitchedAdsInit(adPodArray)
    end if
    m.top.facade.visible = false
    ' start playback for video
    video = m.top.video

    ' Enable Youbora logging

    m.global.addFields({YouboraLogActive: true})
    
    ' > Two methods to define player
    ' First method is set videoplayer reference
    'm.youboraPlugin.videoplayer = m.top.video 'Pass Video reference to Youbora
    
    ' Second method is set an array on updateplayer field
    m.youboraPlugin.updateplayer = {player: m.top.video, unobserveGlobalScope: true}
    
    ' Set plugin options

    m.youboraPlugin.options = GetOptions("StitchedAdMixed", 2, invalid)

    port = createObject("roMessagePort")
    video.observeField("position", port)
    video.observeField("state", port)
    video.visible = true
    video.control = "play"
    video.setFocus(true)
    keepPlaying = true
    ' create video node wrapper object for StitchedAdHandledEvent function
    player = { sgNode: video, port: port }
    ' event-loop
    while keepPlaying
        msg = wait(0, port)
        ' check if we're rendering a stitched ad which handles the event
        curAd = adIface.StitchedAdHandledEvent(msg, player)
        ' ad handled event
        if curAd <> invalid and curAd.evtHandled <> invalid
            if curAd.adExited
                keepPlaying = false
            end if
        else ' no current ad or ad did not handle event, fall through to default event handling
            if curAd = invalid and not video.hasFocus() then video.setFocus(true)
            if type(msg) = "roSGNodeEvent"
                if msg.GetField() = "control"
                    if msg.GetData() = "stop"
                        exit while
                    end if
                else if msg.GetField() = "state"
                    curState = msg.GetData()
                    if curState = "stopped" or curState = "finished"
                        keepPlaying = false
                    end if
                end if
            end if
        end if
    end while
end function

' Get plugin options

function GetOptions(title as string, pattern as integer, package) as object

  youboraOptions                                      = m.youboraPlugin.options
  youboraOptions["expectAds"]                         = "true"
  youboraOptions["anonymousUser"]                     = "hello!"
  youboraOptions["user.email"]                        = "1"
  ' youboraOptions["network.ip"]                        = "1.2.3.4"

  youboraOptions["ad.title"]                          = "adtitle"
  youboraOptions["ad.customDimension.7"]              = "Extra param for ad 7"
  youboraOptions["ad.creativeId"]                     = "adcreativeid123"
  youboraOptions["ad.breaksTime"]                     = [0,14,25]
  youboraOptions["ad.expectedPattern"]                = {
                                                          "pre":  [pattern],
                                                          "mid":  [pattern, pattern, pattern],
                                                          "post": [2]
                                                        }
  youboraOptions["ad.provider"]                       = "provider name"

  if ( package <> invalid )
    youboraOptions["content.package"]                 = package
  end if

  youboraOptions["content.title"]                     = title
  youboraOptions["content.duration"]                  = 600
  youboraOptions["content.transportFormat"]           = "TS"
  youboraOptions["content.streamingProtocol"]         = "HLS"
  youboraOptions["content.metadata"]                  = {
                                                          "year": "2001",
                                                          "genre": "Fantasy",
                                                          "price": "free"
                                                        }
  youboraOptions["content.sendTotalBytes"]            = false
  youboraOptions["content.totalBytes"]                = 288 'Only works if content.sendTotalBytes is set to true, no need to set any value though
  youboraOptions["content.customDimension.1"]         = "Extra param 1 value"
  youboraOptions["content.customDimension.12"]        = "Extra param 12 value"
  youboraOptions["content.saga"]                      = "3"
  youboraOptions["content.tvShow"]                    = "4"
  youboraOptions["content.season"]                    = "5"
  youboraOptions["content.episodeTitle"]              = "6"
  youboraOptions["content.Channel"]                   = "7"
  youboraOptions["content.id"]                        = "8"
  youboraOptions["content.imdbId"]                    = "9"
  youboraOptions["content.gracenoteId"]               = "10"
  youboraOptions["content.type"]                      = "11"
  youboraOptions["content.genre"]                     = "12"
  youboraOptions["content.language"]                  = "13"
  youboraOptions["content.subtitles"]                 = "14"
  youboraOptions["content.contractedResolution"]      = "15"
  youboraOptions["content.cost"]                      = "16"
  youboraOptions["content.price"]                     = "17"
  youboraOptions["content.playbackType"]              = "18"
  youboraOptions["content.drm"]                       = "19"
  youboraOptions["content.encoding.videoCodec"]       = "20"
  youboraOptions["content.encoding.audioCodec"]       = "21"
  youboraOptions["content.encoding.codecSettings"]    = "22"
  youboraOptions["content.encoding.codecProfile"]     = "23"
  youboraOptions["content.encoding.containerFormat"]  = "24"
  youboraOptions["content.customDimensions"]          = {
                                                          "customer dimensions" : "value 1"
                                                        }

  return youboraOptions

end function

function GetAdPods(feedFile as string) as object
    feed = ReadAsciiFile(feedFile)
    result = ParseJson(feed)

    if type(result) <> "roArray"
        return []
    end if

    return result
end function

sub _stateListener()
    if m.top.state <> invalid
        if m.top.state = "stop"
            m.youboraPlugin.event = {handler:"stop"}
            m.youboraPlugin.taskState = m.top.state
        end if
    end if
end sub
